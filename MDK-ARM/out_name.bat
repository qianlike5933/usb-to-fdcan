@echo off
:: 获取日期并将格式设置为：年年月月日日
set datevar=%date:~2,2%%date:~5,2%%date:~8,2%

:: 输出格式化的日期
::echo %datevar%


:: 设置文件路径s
set "filename=..\App\inc\qlk_version.h"

:: 初始化变量
set "version="

:: 使用findstr查找包含V1的行，并使用for循环来提取版本号
for /f "tokens=3 delims= " %%a in ('findstr /c:"#define  V1" "%filename%"') do (
  set "version=%%~a"
)
 
move .\USB2FDCAN\usb-canfd-slave.hex .\USB2FDCAN\usb-canfd-slave_%version%_%datevar%%timevar%.hex
move .\USB2FDCAN\usb-canfd-slave.bin .\USB2FDCAN\usb-canfd-slave_%version%_%datevar%%timevar%.bin

:: 打印提取的版本号
echo Version number is: %version%
