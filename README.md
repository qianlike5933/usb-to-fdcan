# USB-CANFD-SLAVE
## 0. 相关链接
### 0.1 硬件
原理图及 PCB：待整理
### 0.2 下位机
gitee：https://gitee.com/qianlike5933/usb-canfd-slave
### 0.3 上位机（PYQT）
gitee：https://gitee.com/qianlike5933/usb-canfd-host
## 1. 介绍
- 一个简易的 CANFD 分析仪。
- 支持高速 USB2.0，它使用 USB 的 CDC ACM 实现了虚拟串口。
- 由于通过 USB 传给电脑的数据均为 ASCII 码，故可使用串口助手来接收和发送CANFD 信息。
- 可跑满 1M 波特率的 CAN 总线 （5M 未测）。
## 2. 协议介绍
### 2.1 发送（单次）
```
<S> <PORTX> <ID_HEX> [DATA_HEX] [options]\r\n
```
**应答说明：(为提高FDCAN发送帧率，默认无应答，可在config.h文件中给宏SEND_ACK_FLAG解注释开启)**
发送成功返回：`S OK\r\n`
发送失败返回：`S ERR x\r\n`，其中 `x` 表示错误代码。
**例：**
`S 1 8001 1234\r\n` 表示使用默认配置发送 ID 为 0x8001，数据为 0x1234 的 CAN 帧，通过 CAN1 通道发发送。
`S 1 8001 1234 BFrE\r\n` 表示发送 ID 为 0x8001，数据为 0x1234 的 CANFD 帧，其中 ID 类型为拓展 ID，并开启可变波特率，通过 CAN1 通道发发送。
### 2.2 定时发送
```
<T> <T_HEX_MS> <NUM_HEX> <ID_HEX> [DATA_HEX] [options]\r\n
```
**说明：**
-  `T_HEX_MS`：每次间隔（单位：ms）。
-  `NUM_HEX`：发送次数，`X` 表示无数次。
### 2.3 接收
```
<R> <PORTX> <ID_HEX> [DATA_HEX] [options]\r\n
```
### 2.4 配置相关
#### 2.4.1 查询
##### 2.4.1.1 查询所有可以用配置
```
<C> <get> <set>\r\n
```
此指令会返回三行数据：
1. `C get set <CAN_NUM> <NOMI_BAUD_NUM> <DATA_BAUD_NUM>\r\n`
2. `C get nomi [BAUD1] [BAUD2] [BAUD3] [...\r\n`
3. `C get data [BAUD1] [BAUD2] [BAUD3] [...\r\n`
**例：**
```
send:C get set\r\n
recv:C get all 1 7 8\r\n
recv:C get nomi 1000 800 500 250 125 100 50\r\n
recv:C get data 5000 4000 2000 1000 800 500 250 125\r\n
```
1. 接收第一行：1 个 CAN 通道，仲裁段有 7 个波特率可选，数据段有8个波特率可选。
2. 接收第二行：仲裁段 7 个波特率分别为（单位为 kpbs）：1000 800 500 250 125 100 50。
3. 接收第三行：数据段 8 个波特率分别为（单位为 kpbs）：5000 4000 2000 1000 800 500 250 125。
##### 2.4.1.2 查询当前使用配置
```
<C> <get> <config>\r\n
```
此指令返回的行数等于 CAN 通道数。
**例：**
```
send:C get config\r\n
recv:C get config 1 1000 5000 BFERS\r\n
recv:C get config 2 800 4000 BFERS\r\n
```
1. 接收第一行：CAN 通道 1 仲裁段波特率为 1000Kpbs，数据段波特率为 5000Kbps。
2. 接收第一行：CAN 通道 2 仲裁段波特率为 800Kpbs，数据段波特率为 4000Kbps。
3. 可变参数解析：设置为 FDCAN 模式，开启可变波特率，使能 CAN 口，开启发送失败后自动重发，使能终端电阻。
#### 2.4.2 设置
##### 2.4.2.1 设置波特率
```
<C> <set> <fix> <CAN_PORT/all> <NOMI_BAUD> <DATA_BAUD> [options2]\r\n
```
1. `CAN_PORT/all`：CAN 通道，从 1 开始， `all` 为全部通道。
2. `NAMI_BAUD`：仲裁段波特率。
3. `DATA_BAUD`：数据段波特率。
4. `options2`：可选参数。
**例：**
```
C set fix all 1000 5000 FbESr\r\n
```
- 设置所有 CAN 通道的仲裁段波特率为 1000，数据段波特率为 5000
- 可变参数解析：设置为 FDCAN 模式，关闭可变波特率（即数据段波特率和仲裁段一样），使能 CAN 口，开启发送失败后自动重发，失能终端电阻。
##### 2.4.2.2 自定义波特率（FDCAN 时钟频率为 80MHz）
```
<C> <set> <cus> <CAN_PORT/all> <NOMI_PRE> <NOMI_SJW> <NOMI_TSEG1> <NOMI_TSEG2> <DATA_PRE> <DATA_SJW> <DATA_TSEG1> <DATA_TSEG2> [options2]\r\n
```
1. `CAN_PORT/all`：CAN 通道，从 1 开始， `all` 为全部通道。
2. `NOMI_PRE`：仲裁段，预分频器
3. `NOMI_SJW`：仲裁段，同步跳跃宽度
4. `NOMI_TSG1`：仲裁段，时间段 1
5. `NOMI_TSG2`：仲裁段，时间段 2
6. `DATA_PRE`：数据段，预分频器
7. `DATA_SJW`：数据段，同步跳跃宽度
8. `DATA_TSEG1`：数据段，时间段 1
9. `DATA_TSEG2`：数据段，时间段 2
**例：**
```
C set cus all 2 8 31 8 2 2 5 2 FbESr\r\n
```
- 该例子为仲裁段波特率 1M，数据段波特率为 5M。
- 设置为 FDCAN 模式，关闭可变波特率（即数据段波特率和仲裁段一样），使能 CAN 口，开启发送失败后自动重发，失能终端电阻。
##### 2.4.2.3 开关 CAN 通道
```
<C> <set> <init> <CAN_PORT/all> <0/1>\r\n
```
1. `CAN_PORT/all`：CAN 通道，从 1 开始， `all` 为全部通道。
2. `0/1`：0-关闭，1-开启。
**注：**
- 此指令只会控制 CAN 通道开关，不会更改其他配置。
- 此指令没有可选参数。
**例：**
```
C set inti all 1\r\n
```
### 2.5 查看固件版本
```
<V>\r\n
```

### 2.7 可选参数介绍
#### 2.7.1 可选参数 1 介绍（用于发送和接收）
可选参数 `options` 介绍：`options` 可以是一个或多个。
- `B/b`：开启/禁用 比特率切换。
- `F/f`：开启/禁用 FDCAN 格式。
- `R/r`：开启/禁用 远程帧。
- `E/e`：开启/禁用 拓展 ID。
- `Hx`：CAN 通道选择（例如 H1 表示使用 CAN1 通道发送数据）（现硬件限制，仅有H2，待硬件更新）。
- `Txxxx`：接收时间戳（格式和 ID 一样，如：`123` 表示 `291us`, `F123` 表示 `61731us`），单位 us。
#### 2.7.2 可选参数 2 介绍（用于设置）
可选参数 `options2` 介绍：`options2` 可以是一个或多个。
- `B/b`：开启/禁用比特率切换。
- `F/f`：开启/禁用 FDCAN 格式。
- `E/e`：使能/失能 CAN 通道。
- `S/s`：开启/禁用发送失败自动重发。
- `R/r`：使能/失能终端电阻（待硬件更新）。

## 3. 更新计划
- [x] 更新协议。
- [x] 修复 DEBUG_PRINT 的 BUG。
- [ ] 定时发送功能（进行中）。
- [x] 接收时间戳。
- [ ] 支持 10 进制。
- [ ] 获取 CAN 发送时间。
- [x] 设置
    - [x] 模式：CAN、FDCAN
    - [x] 波特率（单位bps）：
        - [x] 仲裁段：
            - [x] 1M
            - [x] 800K
            - [x] 500K
            - [x] 250K
            - [x] 125K
            - [x] 100K
            - [x] 50k
        - [x] 数据段：
            - [x] 5M
            - [x] 4M
            - [x] 2M
            - [x] 1M
            - [x] 800k
            - [x] 500k
            - [x] 250k
            - [x] 125k
    - [x] 可变波特率
    - [x] 使能终端电阻（待硬件更新）
    - [x] 开关CAN 通道功能。
    - [x] 自定义波特率
    - [x] 保存为默认配置
    - [x] 计算自定义波特率。
    - [x] 查看当前配置（波特率）
- [x] 加入查看固件版本功能。
