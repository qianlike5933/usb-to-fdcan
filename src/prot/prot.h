#ifndef _PROT_H
#define _PROT_H

#include "qlk_fdcan.h"
#include "qlk_usart.h"


void Prot_Fdcan_To_Usart(void);
void Prot_Usart_To_Fdcan(void);

#endif
