#include "prot.h"
#include "qlk_version.h"
#include "fdcan_config.h"
#include "fdcan_reg.h"
#include "conversion.h"



void Prot_Fdcan_To_Usart()
{
    fdcan_buf fdcan2_buf;
    static uint8_t usart1_tdata[USART_BUF_LEN] = {'R', ' '};

    if (Fdcan_Queue_Pop(&fdcan2_buf) == 0)
    {
        uint16_t len = Conv_Hex_To_Ascii(&fdcan2_buf, &usart1_tdata[2]);
        if (len != 0)
        {
            Usart_Send(usart1_tdata, len + 2);
        }
    }
}


void Prot_Usart_To_Fdcan()
{
    uint8_t len = 0;
    static uint8_t usart_buf[USART_BUF_LEN] = {0};
    static fdcan_buf fdcan2_buf;

    len = Usart_Queue_Pop(usart_buf);
    if (len == 0)
    {
        return;
    }

    switch (usart_buf[0])
    {
    case ('S'):
    case ('s'):
        if (Conv_Ascii_To_Hex(&usart_buf[2], &fdcan2_buf) == 0)
        {
            Fdcan_Port_Send(&fdcan2_buf, Fdcan_Config_Get_Acc(fdcan2_buf.port));
        }
        break;
    case ('V'):
    case ('v'):
        Usart_Send((uint8_t *)(VERSION), sizeof(VERSION) - 1);
        break;
    case ('C'):
    case ('c'):
        Config_Handle((char *)&usart_buf[2]);
        break;
    case ('T'):
    case ('t'):
        Reg_Handle((char *)&usart_buf[2]);
        break;
    default:
        break;
    }
}

