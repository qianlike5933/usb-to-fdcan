/**
 * 此文件暂使用自己写的转换函数，后续可能会更换为库函数
 */


#include "conversion.h"


uint8_t Conv_Str_Int(char **pp_str, uint32_t *p_data)
{
    if (pp_str == NULL || *pp_str == NULL || p_data == NULL)
    {
        DEBUG_PRINT("NULL!!!\r\n");
        return 1;
    }
    
    char *p_str = *pp_str;
    *p_data = 0;
    do
    {
        *p_data <<= 4;
        if (*p_str >= '0' && *p_str <= '9')
        {
            *p_data += *p_str - '0';
        }
        else if (*p_str >= 'A' && *p_str <= 'F')
        {
            *p_data += *p_str - 'A' + 10;
        }
        else if (*p_str >= 'a' && *p_str <= 'f')
        {
            *p_data += *p_str - 'a' + 10;
        }
        else
        {
            DEBUG_PRINT("ERR: str to int err!!!");
            return 2;
        }
        ++p_str;
    }
    while (*p_str != ' ' && *p_str != '\r' && *p_str != '\n');
    ++p_str;
    *pp_str = p_str;

    return 0;
}


uint16_t Conv_Hex_To_Ascii(fdcan_buf *p_fdcan_buf, uint8_t *p_str_start)
{
    const char hex_map[] = "0123456789ABCDEF";
    uint8_t *p_str = p_str_start;

    if (p_fdcan_buf == NULL || p_str == NULL)
    {
        DEBUG_PRINT("ERR: Pointer is empty!!!");
        return 0;
    }

    /* 处理 CAN 通道 */
    *p_str++ = p_fdcan_buf->port + '0';
    *p_str++ = ' ';

    /* 处理 ID */
    uint32_t id = p_fdcan_buf->id;
    uint8_t temp[9] = {0};
    uint8_t ii = 8;
    temp[8] = ' ';
    do
    {
        temp[--ii] = hex_map[id & 0x0F];
        id >>= 4;
    }
    while (id != 0);
    memcpy(p_str, &temp[ii], 9 - ii);
    p_str += 9 - ii;


    /* 处理数据段 */
    if (p_fdcan_buf->len != 0)
    {
        for (uint16_t i = 0; i < p_fdcan_buf->len; ++i)
        {
            *p_str++ = hex_map[p_fdcan_buf->data[i] >> 4];
            *p_str++ = hex_map[p_fdcan_buf->data[i] & 0x0F];
        }
        *p_str++ = ' ';
    }

    /* 处理标志段 */
    *p_str++ = p_fdcan_buf->id_type == 1 ? 'E' : 'e';
    *p_str++ = p_fdcan_buf->bit_rate == 1 ? 'B' : 'b';
    *p_str++ = p_fdcan_buf->can_type == 1 ? 'F' : 'f';
    *p_str++ = p_fdcan_buf->frame_type == 1 ? 'R' : 'r';
    *p_str++ = 'H';
    *p_str++ = p_fdcan_buf->port + '0';
    *p_str++ = 'T';
    ii = 5;
    uint16_t t = p_fdcan_buf->time;
    do
    {
        temp[--ii] = hex_map[t & 0x0F];
        t >>= 4;
    }
    while (t != 0);
    memcpy(p_str, &temp[ii], 5 - ii);
    p_str += 5 - ii;

    *p_str++ = '\r';
    *p_str++ = '\n';

    return (uint16_t)(p_str - p_str_start);
}

uint8_t Conv_Ascii_To_Hex(uint8_t *p_str_start, fdcan_buf *p_fdcan_buf)
{
    uint8_t *p_str = p_str_start;
    int8_t temp = 0;

    if (p_fdcan_buf == NULL || p_str == NULL)
    {
        DEBUG_PRINT("ERR: Pointer is empty!!!");
        return 1;
    }

    if (!(*p_str >= '0' && *p_str <= '9'))
    {
        DEBUG_PRINT("ERR: can port err!!!");
        return 2;
    }
    p_fdcan_buf->port = *p_str - '0';
    p_str += 2;

    /* 处理 ID */
    p_fdcan_buf->id = 0;
    do
    {
        p_fdcan_buf->id <<= 4;
        if (*p_str >= '0' && *p_str <= '9')
        {
            p_fdcan_buf->id += *p_str - '0';
        }
        else if (*p_str >= 'A' && *p_str <= 'F')
        {
            p_fdcan_buf->id += *p_str - 'A' + 10;
        }
        else if (*p_str >= 'a' && *p_str <= 'f')
        {
            p_fdcan_buf->id += *p_str - 'a' + 10;
        }
        else
        {
            DEBUG_PRINT("ERR: ID error!!!");
            return 3;
        }
    }
    while (*++p_str != ' ');
    ++p_str;
    p_fdcan_buf->len = 0;
    while (*p_str != ' ' && *p_str != '\r' && *p_str != '\n')
    {
        temp = 0;
        for (int i = 0; i < 2; ++i, ++p_str)
        {
            temp <<= 4;
            if (*p_str >= '0' && *p_str <= '9')
            {
                temp += *p_str - '0';
            }
            else if (*p_str >= 'A' && *p_str <= 'F')
            {
                temp += *p_str - 'A' + 10;
            }
            else if (*p_str >= 'a' && *p_str <= 'f')
            {
                temp += *p_str - 'a' + 10;
            }
            else
            {
                DEBUG_PRINT("ERR: data error!!!");
                return 4;
            }

        }
        p_fdcan_buf->data[p_fdcan_buf->len++] = temp;
    }

    /* 参数处理 */
    if (*p_str++ == ' ')
    {
        p_fdcan_buf->config_flag = 1;
        while (*p_str != '\r' && *p_str != '\n')
        {
            switch (*p_str++)
            {
            case ('b'):
                p_fdcan_buf->bit_rate = 0;
                break;
            case ('e'):
                p_fdcan_buf->id_type  = 0;
                break;
            case ('f'):
                p_fdcan_buf->can_type = 0;
                break;
            case ('r'):
                p_fdcan_buf->frame_type = 0;
                break;
            case ('B'):
                p_fdcan_buf->bit_rate = 1;
                break;
            case ('E'):
                p_fdcan_buf->id_type  = 1;
                break;
            case ('F'):
                p_fdcan_buf->can_type = 1;
                break;
            case ('R'):
                p_fdcan_buf->frame_type = 1;
                break;
            case ('H'):
                p_fdcan_buf->port = *p_str++;
                break;
            }
        }
    }
    return 0;
}
