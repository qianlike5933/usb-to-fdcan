#ifndef _CONVERSION_H
#define _CONVERSION_H


#include "qlk_fdcan.h"
#include "qlk_usart.h"


uint8_t Conv_Str_Int(char **pp_str, uint32_t *p_data);
uint16_t Conv_Hex_To_Ascii(fdcan_buf *p_fdcan_buf, uint8_t *p_str_start);
uint8_t Conv_Ascii_To_Hex(uint8_t *p_str_start, fdcan_buf *p_fdcan_buf);


#endif
