#ifndef _FDCAN_REG_H
#define _FDCAN_REG_H


#include "qlk_fdcan.h"


#define  REG_OFFSET_EN          0
#define  REG_OFFSET_ACC         1
#define  REG_OFFSET_NUM_LIMI    2
#define  REG_OFFSET_DEL         3


typedef struct
{
    fdcan_buf buf;
    uint32_t cnt;        // 发送次数
    uint16_t inte;       // 每次间隔时间
    uint32_t old_time;  // 下次要发送的时间
    uint8_t flag;        // 0位-使能，1位-发送反馈，2位-是否有次数限制，3位-到次数后是否删除节点
} fdcan_reg_s, *p_fdcan_reg_s;


typedef struct fdcan_reg_list_s
{
    fdcan_reg_s reg;
    struct fdcan_reg_list_s *next;
} fdcan_reg_list_s, *p_fdcan_reg_list_s;


void Reg_Init(void);
void Reg_Handle(char *p_str);
void Reg_Run(void);



#endif
