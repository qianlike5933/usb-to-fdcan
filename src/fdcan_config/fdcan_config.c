#include "fdcan_config.h"
#include "qlk_flash.h"
#include <string.h>




const fdcan_config_handle_s fdcan_config_handle[] =   // CAN 通道映射
{
    // {
    //     .p_fdcan_x = &hfdcan1,
    //     .instance = FDCAN1,
    // },

    {
        .p_fdcan_x = &hfdcan2,
        .p_instance = FDCAN2,
    },

    // {
    //     .p_fdcan_x = &hfdcan3,
    //     .instance = FDCAN3,
    // },
};

const fdcan_config_baud_s fdcan_config_nomi[] =  // 仲裁域波特率
{
    {
        .baud = 1000,
        .pre = 2,
        .sjw = 8,
        .tseg1 = 31,
        .tseg2 = 8,
    },

    {
        .baud = 800,
        .pre = 2,
        .sjw = 8,
        .tseg1 = 38,
        .tseg2 = 10,
    },

    {
        .baud = 500,
        .pre = 4,
        .sjw = 8,
        .tseg1 = 31,
        .tseg2 = 8,
    },

    {
        .baud = 250,
        .pre = 8,
        .sjw = 8,
        .tseg1 = 31,
        .tseg2 = 8,
    },

    {
        .baud = 125,
        .pre = 16,
        .sjw = 8,
        .tseg1 = 31,
        .tseg2 = 8,
    },

    {
        .baud = 100,
        .pre = 20,
        .sjw = 8,
        .tseg1 = 31,
        .tseg2 = 8,
    },

    {
        .baud = 50,
        .pre = 40,
        .sjw = 8,
        .tseg1 = 31,
        .tseg2 = 8,
    },
};

const fdcan_config_baud_s fdcan_config_data[] =  // 数据域波特率
{
    {
        .baud = 5000,
        .pre = 2,
        .sjw = 2,
        .tseg1 = 5,
        .tseg2 = 2,
    },

    {
        .baud = 4000,
        .pre = 2,
        .sjw = 2,
        .tseg1 = 7,
        .tseg2 = 2,
    },

    {
        .baud = 2000,
        .pre = 4,
        .sjw = 2,
        .tseg1 = 7,
        .tseg2 = 2,
    },

    {
        .baud = 1000,
        .pre = 8,
        .sjw = 2,
        .tseg1 = 7,
        .tseg2 = 2,
    },

    {
        .baud = 800,
        .pre = 10,
        .sjw = 2,
        .tseg1 = 7,
        .tseg2 = 2,
    },

    {
        .baud = 500,
        .pre = 16,
        .sjw = 2,
        .tseg1 = 7,
        .tseg2 = 2,
    },

    {
        .baud = 250,
        .pre = 32,
        .sjw = 2,
        .tseg1 = 7,
        .tseg2 = 2,
    },

    {
        .baud = 125,
        .pre = 32,
        .sjw = 2,
        .tseg1 = 15,
        .tseg2 = 4,
    },
};

fdcan_config_s fdcan_config[sizeof(fdcan_config_handle) / sizeof(fdcan_config_handle[0])] = 
{
	{
        .s.para = 
        {
            .bit_rate = 0xFF,
            .can_type = 0xFF,
            .enable = 0xFF,
            .send_res = 0xFF,
            .res_flag = 0xFF,
            .send_acc = 0xFF,
        },
    },
};


uint8_t Fdcan_Config_Get_Acc(const uint8_t x)
{
    return fdcan_config[x].s.para.send_acc;
}


void Fdcan_Config_Read()
{
    Flash_Read_int64(FDCAN_CONFIG_ADDR, fdcan_config->data64, sizeof(fdcan_config->data64) / sizeof(uint64_t));
}


static void Fdcan_Config_Write()
{
    Flash_Write_int64(FDCAN_CONFIG_ADDR, fdcan_config->data64, sizeof(fdcan_config->data64) / sizeof(uint64_t));
}


void Fdcan_Init()
{
    FDCAN_HandleTypeDef *p_fdcan_x = NULL;

    for (uint8_t i = 0; i < sizeof(fdcan_config_handle) / sizeof(fdcan_config_handle[0]); i++)
    {
        p_fdcan_x = fdcan_config_handle[i].p_fdcan_x;
        p_fdcan_x->Instance = fdcan_config_handle[i].p_instance;

		if (fdcan_config[i].s.para.bit_rate == 0xFF && fdcan_config[i].s.para.can_type == 0xFF 
            && fdcan_config[i].s.para.enable == 0xFF && fdcan_config[i].s.para.send_res == 0xFF)  // 默认 仲裁域 1M，数据域 5M，FDCAN，开启自动重发
        {
            fdcan_config[i].s.nomi.pre = 2;
            fdcan_config[i].s.nomi.sjw = 8;
            fdcan_config[i].s.nomi.tseg1 = 31;
            fdcan_config[i].s.nomi.tseg2 = 8;

            fdcan_config[i].s.data.pre = 2;
            fdcan_config[i].s.data.sjw = 2;
            fdcan_config[i].s.data.tseg1 = 5;
            fdcan_config[i].s.data.tseg2 = 2;

            fdcan_config[i].s.para.enable = 1;
            fdcan_config[i].s.para.can_type = 1;
            fdcan_config[i].s.para.bit_rate = 1;
            fdcan_config[i].s.para.send_res = 1;
            fdcan_config[i].s.para.res_flag = 1;

            Fdcan_Config_Write();
        }
        
        p_fdcan_x->Init.NominalPrescaler = fdcan_config[i].s.nomi.pre;   
        p_fdcan_x->Init.NominalSyncJumpWidth = fdcan_config[i].s.nomi.sjw;   
        p_fdcan_x->Init.NominalTimeSeg1 = fdcan_config[i].s.nomi.tseg1; 
        p_fdcan_x->Init.NominalTimeSeg2 = fdcan_config[i].s.nomi.tseg2; 

        p_fdcan_x->Init.DataPrescaler = fdcan_config[i].s.data.pre;
        p_fdcan_x->Init.DataSyncJumpWidth = fdcan_config[i].s.data.sjw;
        p_fdcan_x->Init.DataTimeSeg1 = fdcan_config[i].s.data.tseg1;
        p_fdcan_x->Init.DataTimeSeg2 = fdcan_config[i].s.data.tseg2;

        p_fdcan_x->Init.FrameFormat = fdcan_config[i].s.para.can_type != 0 ? fdcan_config[i].s.para.bit_rate != 0 ? 
                                        FDCAN_FRAME_FD_BRS : FDCAN_FRAME_FD_NO_BRS : FDCAN_FRAME_CLASSIC;
        p_fdcan_x->Init.AutoRetransmission = fdcan_config[i].s.para.send_res != 0 ? ENABLE : DISABLE;

        p_fdcan_x->Init.StdFiltersNbr = 0;
        p_fdcan_x->Init.ExtFiltersNbr = 0;
        p_fdcan_x->Init.ClockDivider = FDCAN_CLOCK_DIV1;
        p_fdcan_x->Init.Mode = FDCAN_MODE_NORMAL;
        p_fdcan_x->Init.TransmitPause = DISABLE;
        p_fdcan_x->Init.ProtocolException = ENABLE;

        if (fdcan_config[i].s.para.enable == 0)
        {
            HAL_FDCAN_MspDeInit(p_fdcan_x);
            return;
        }
        
        if (HAL_FDCAN_Init(p_fdcan_x) != HAL_OK)
        {
            Error_Handler();
        }

        HAL_FDCAN_ConfigTimestampCounter(p_fdcan_x, FDCAN_TIMESTAMP_PRESC_1);
        HAL_FDCAN_EnableTimestampCounter(p_fdcan_x, FDCAN_TIMESTAMP_INTERNAL);
        fdcan_filter_init(p_fdcan_x);
    }
}


static void Config_Get(char *p_str)
{
    char tdata[100] = {0};
    uint8_t len = 0;

    if (strncmp(p_str, "set", 3) == 0)
    {
        print("C get set %d %d %d\r\n", 
              sizeof(fdcan_config_handle) / sizeof(fdcan_config_handle[0]),
              sizeof(fdcan_config_nomi) / sizeof(fdcan_config_nomi[0]),
              sizeof(fdcan_config_data) / sizeof(fdcan_config_data[0]));
        HAL_Delay(5);

        len = 11;
        memcpy(tdata, "C get nomi ", len);
        for (uint8_t i = 0; i < sizeof(fdcan_config_nomi) / sizeof(fdcan_config_nomi[0]); i++)
        {
            len += snprintf(tdata + len, sizeof(tdata) - len, "%d ", fdcan_config_nomi[i].baud);
        }
        len += snprintf(tdata + len, sizeof(tdata) - len, "\r\n");
        Usart_Send((uint8_t *)tdata, len);

        len = 11;
        memcpy(tdata, "C get data ", len);
        for (uint8_t i = 0; i < sizeof(fdcan_config_data) / sizeof(fdcan_config_data[0]); i++)
        {
            len += snprintf(tdata + len, sizeof(tdata) - len, "%d ", fdcan_config_data[i].baud);
        }
        len += snprintf(tdata + len, sizeof(tdata) - len, "\r\n");
        Usart_Send((uint8_t *)tdata, len);

        return;
    }
    else if (strncmp(p_str, "config", 6) == 0)
    {
        for (uint8_t i = 0; i < sizeof(fdcan_config_handle) / sizeof(fdcan_config_handle[0]); i++)
        {
            const uint8_t c_bit_rate = fdcan_config[i].s.para.bit_rate != 0 ? 'B' : 'b';
            const uint8_t c_can_type = fdcan_config[i].s.para.can_type != 0 ? 'F' : 'f';
            const uint8_t c_enable = fdcan_config[i].s.para.enable != 0 ? 'E' : 'e';
            const uint8_t c_send_res = fdcan_config[i].s.para.send_res != 0 ? 'S' : 's';
            const uint8_t c_res_flag = fdcan_config[i].s.para.res_flag != 0 ? 'R' : 'r';
            const uint8_t send_acc = fdcan_config[i].s.para.send_acc != 0 ? 'A' : 'a';
            print("C get config %d %d %d %c%c%c%c%c%c\r\n", i + 1, fdcan_config[i].s.nomi.baud, fdcan_config[i].s.data.baud,
                                                        c_bit_rate, c_can_type, c_enable, c_res_flag, c_send_res, send_acc);
            HAL_Delay(2);
        }

        return;
    }

    print("C get err\r\n");
}


/**
 * @brief ASCII 转 int, 十进制转换
 * @param pp_str 指向十进制字符串指针的指针
 * @return 十进制的 int 类型
 */
static uint16_t Config_Ascii_To_Int(char **pp_str)
{
    uint16_t r = 0;
    char *p_str = *pp_str;

    while (*p_str != ' ' && *p_str != '\r')
    {
        r = r * 10 + (*p_str) - '0';
        p_str++;
    }

    *pp_str = p_str + 1;

    return r;
}


/**
 * @brief 在 fdcan_config_baud_s 中查找所需波特率的下标
 * @param buad 所需波特率
 * @param p_fdcan_config 目标 fdcan_config_baud_s 结构体
 * @param len 结构体数组的长度
 * @return 返回 0xFF 表示未找到，返回非 0xFF 的值表示所需波特率所在下标
 */
static uint8_t Config_Fild_Buad(const uint16_t buad, const fdcan_config_baud_s *p_fdcan_config, const uint8_t len)
{
    for (uint8_t i = 0; i < len; i++)
    {
        if (p_fdcan_config[i].baud == buad)
        {
            return i;
        }
    }

    return 0xFF;
}


/**
 * @brief 获取指令中的 CANX
 * @param pp_str 指令指针的指针
 * @param p_canx 保存 CAN 通道的数组
 * @param p_can_num CAN 通道的数量
 * @return 0-成功，1-失败
 */
static uint8_t Config_Set_Inspect_Canx(char **pp_str, uint8_t *p_canx, uint8_t *p_can_num)
{
    char *p_str = *pp_str;

    if (strncmp(p_str, "all", 3) == 0)
    {
        *pp_str += 4;
        *p_can_num = sizeof(fdcan_config_handle) / sizeof(fdcan_config_handle[0]);
        for (uint8_t i = 0; i < *p_can_num; i++)
        {
            p_canx[i] = i + 1;
        }
    }
    else
    {
        *p_can_num = 1;
        p_canx[0] = Config_Ascii_To_Int(&p_str);
		*pp_str = p_str;
        if (p_canx[0] > sizeof(fdcan_config_handle) / sizeof(fdcan_config_handle[0]))
        {
            print("C set canx err\r\n");
            return 1;
        }
    }

    return 0;
}


static void Config_Calculate_Baud_kbps(const uint32_t fdcan_clk_mhz, fdcan_config_baud_s *p)
{
    const uint32_t total_tq = 1 + p->tseg1 + p->tseg2;
    p->baud = (fdcan_clk_mhz * 1000000 / (p->pre * total_tq)) / 1000;
}


/**
 * @brief 
 * @param p_nomi 
 * @param p_data 
 * @param diff
 * @return 
 */
static uint8_t Config_Nomi_Data_Para_Validate(const fdcan_config_baud_s *p_nomi, const fdcan_config_baud_s *p_data)
{
    /*-------------------------------------------
    错误代码定义（返回值说明）：
    0  - 参数有效
    1  - 标称段预分频器超出范围(1-256)
    2  - 标称段SJW超出范围(1-128)
    3  - 标称段TSEG1超出范围(2-256)
    4  - 标称段TSEG2超出范围(1-128)
    5  - 标称段SJW > min(TSEG1,TSEG2)
    6  - 标称段总时间片超限(4-256)
    7  - 数据段预分频器超出范围(1-32)
    8  - 数据段SJW超出范围(1-16)
    9  - 数据段TSEG1超出范围(2-32)
    10 - 数据段TSEG2超出范围(1-16)
    11 - 数据段SJW > min(TSEG1,TSEG2)
    12 - 数据段总时间片超限(4-32)
    13 - 标称段采样点异常(推荐70%~87.5%)
    14 - 数据段采样点异常(推荐70%~87.5%)
    -------------------------------------------*/

    /*==================== 标称段参数验证 ====================*/
    // 预分频器校验 (NBTP.NBRP范围)
    if(p_nomi->pre < 1)  //  || p_nomi->pre > 256
    {
        return 1;
    }
    
    // 同步跳转宽度校验 (NBTP.NSJW范围)
    if(p_nomi->sjw < 1 || p_nomi->sjw > 128)
    {
        return 2;
    }
    
    // 时间段1校验 (NBTP.NTSEG1 = tseg1 - 1)
    if(p_nomi->tseg1 < 2)  // || p_nomi->tseg1 > 256
    {
        return 3;
    }
    
    // 时间段2校验 (NBTP.NTSEG2 = tseg2 - 1)
    if(p_nomi->tseg2 < 1 || p_nomi->tseg2 > 128)
    {
        return 4;
    }
    
    // SJW不能超过TSEG1/TSEG2最小值
    if(p_nomi->sjw > (p_nomi->tseg1 < p_nomi->tseg2 ? p_nomi->tseg1 : p_nomi->tseg2))
    {
        return 5;
    }
    
    // 总时间片校验 (SyncSeg(1) + TSEG1 + TSEG2)
    uint16_t total_tq_nomi = 1 + p_nomi->tseg1 + p_nomi->tseg2;
    if(total_tq_nomi < 4 || total_tq_nomi > 256)
    {
        return 6;
    }

    /*==================== 数据段参数验证 ====================*/
    // 预分频器校验 (DBTP.DBRP范围)
    if(p_data->pre < 1 || p_data->pre > 32)
    {
        return 7;
    }
    
    // 同步跳转宽度校验 (DBTP.DSJW范围)
    if(p_data->sjw < 1 || p_data->sjw > 16)
    {
        return 8;
    }
    
    // 时间段1校验 (DBTP.DTSEG1 = tseg1 - 1)
    if(p_data->tseg1 < 2 || p_data->tseg1 > 32)
    {
        return 9;
    }
    
    // 时间段2校验 (DBTP.DTSEG2 = tseg2 - 1)
    if(p_data->tseg2 < 1 || p_data->tseg2 > 16)
    {
        return 10;
    }
    
    // SJW不能超过TSEG1/TSEG2最小值
    if(p_data->sjw > (p_data->tseg1 < p_data->tseg2 ? p_data->tseg1 : p_data->tseg2))
    {
        return 11;
    }
    
    // 总时间片校验 (SyncSeg(1) + TSEG1 + TSEG2)
    uint8_t total_tq_data = 1 + p_data->tseg1 + p_data->tseg2;
    if(total_tq_data < 4 || total_tq_data > 32)
    {
        return 12;
    }

    /*==================== 采样点验证 ====================*/
    // 标称段采样点计算 (SyncSeg不算入采样点)
    float sample_point_nomi = (1.0f + p_nomi->tseg1) / total_tq_nomi;
    if(sample_point_nomi < 0.70f || sample_point_nomi > 0.875f)
    {
        return 13;
    }
    
    // 数据段采样点验证（仅当使用高速模式时检查）
    if(p_data->pre > 1) 
    {
        float sample_point_data = (1.0f + p_data->tseg1) / total_tq_data;
        if(sample_point_data < 0.70f || sample_point_data > 0.875f)
        {
            return 14;
        }
    }

    return 0;
}


static void Config_Set(char *p_str)
{
    uint8_t canx[sizeof(fdcan_config_handle) / sizeof(fdcan_config_handle[0])] = {0};
    uint8_t can_num = 1;
    fdcan_config_para_s para_temp = {0xFF, 0xFF, 0xFF, 0xFF, 0xFF};

    if (strncmp(p_str, "fix", 3) == 0)  // 固定配置
    {
        p_str += 4;
        if (Config_Set_Inspect_Canx(&p_str, canx, &can_num) != 0)
        {
            return;
        }
        
        const uint16_t nomi_baud = Config_Ascii_To_Int(&p_str);
        const uint16_t data_baud = Config_Ascii_To_Int(&p_str);

        const uint8_t nomi_index = Config_Fild_Buad(nomi_baud, fdcan_config_nomi, sizeof(fdcan_config_nomi) / sizeof(fdcan_config_nomi[0]));
        const uint8_t data_index = Config_Fild_Buad(data_baud, fdcan_config_data, sizeof(fdcan_config_data) / sizeof(fdcan_config_data[0]));

        if (nomi_index == 0xFF)
        {
            print("C set nomi err\r\n");
            return;
        }

        if (data_index == 0xFF)
        {
            print("C set data err\r\n");
            return;
        }


        while (*p_str != '\r' && *p_str != '\n')
        {
            switch (*p_str++)
            {
            case ('b'):  para_temp.bit_rate = 0;  break;
            case ('f'):  para_temp.can_type = 0;  break;
            case ('e'):  para_temp.enable = 0;    break;
            case ('s'):  para_temp.send_res = 0;  break;
            case ('r'):  para_temp.res_flag = 0;  break;
            case ('a'):  para_temp.send_acc = 0;  break;

            case ('B'):  para_temp.bit_rate = 1;  break;
            case ('F'):  para_temp.can_type = 1;  break;
            case ('E'):  para_temp.enable = 1;    break;
            case ('S'):  para_temp.send_res = 1;  break;
            case ('R'):  para_temp.res_flag = 1;  break;
            case ('A'):  para_temp.send_acc = 1;  break;
            }
        }

        for (int i = 0; i < can_num; i++)
        {
            fdcan_config[i].s.nomi.baud = fdcan_config_nomi[nomi_index].baud;
            fdcan_config[i].s.nomi.pre = fdcan_config_nomi[nomi_index].pre;
            fdcan_config[i].s.nomi.sjw = fdcan_config_nomi[nomi_index].sjw;
            fdcan_config[i].s.nomi.tseg1 = fdcan_config_nomi[nomi_index].tseg1;
            fdcan_config[i].s.nomi.tseg2 = fdcan_config_nomi[nomi_index].tseg2;

            fdcan_config[i].s.data.baud = fdcan_config_data[data_index].baud;
            fdcan_config[i].s.data.pre = fdcan_config_data[data_index].pre;
            fdcan_config[i].s.data.sjw = fdcan_config_data[data_index].sjw;
            fdcan_config[i].s.data.tseg1 = fdcan_config_data[data_index].tseg1;
            fdcan_config[i].s.data.tseg2 = fdcan_config_data[data_index].tseg2;

            fdcan_config[i].s.para.bit_rate = para_temp.bit_rate;
            fdcan_config[i].s.para.can_type = para_temp.can_type;
            fdcan_config[i].s.para.enable = para_temp.enable;
            fdcan_config[i].s.para.send_res = para_temp.send_res;
            fdcan_config[i].s.para.res_flag = para_temp.res_flag;
            fdcan_config[i].s.para.send_acc = para_temp.send_acc;
        }

        Fdcan_Init();
        print("C set fix ok\r\n");
        return;
    }
    else if (strncmp(p_str, "cus", 3) == 0)  // 自定义配置
    {
        p_str += 4;
        if (Config_Set_Inspect_Canx(&p_str, canx, &can_num) != 0)
        {
            return;
        }

        fdcan_config_baud_s nomi =
        {
            .pre = Config_Ascii_To_Int(&p_str),
            .sjw = Config_Ascii_To_Int(&p_str),
            .tseg1 = Config_Ascii_To_Int(&p_str),
            .tseg2 = Config_Ascii_To_Int(&p_str),
        };

        fdcan_config_baud_s data =
        {
            .pre = Config_Ascii_To_Int(&p_str),
            .sjw = Config_Ascii_To_Int(&p_str),
            .tseg1 = Config_Ascii_To_Int(&p_str),
            .tseg2 = Config_Ascii_To_Int(&p_str),
        };

        if (Config_Nomi_Data_Para_Validate(&nomi, &data) != 0)  // 参数检测
        {
            print("C cus err!!!\r\n");
            return;
        }

        /* 计算波特率 */
        Config_Calculate_Baud_kbps(FDCAN_CLK_MHZ, &nomi);
        Config_Calculate_Baud_kbps(FDCAN_CLK_MHZ, &data);

        while (*p_str != '\r' && *p_str != '\n')
        {
            switch (*p_str++)
            {
            case ('b'):  para_temp.bit_rate = 0;  break;
            case ('f'):  para_temp.can_type = 0;  break;
            case ('e'):  para_temp.enable = 0;    break;
            case ('s'):  para_temp.send_res = 0;  break;
            case ('r'):  para_temp.res_flag = 0;  break;
            case ('a'):  para_temp.send_acc = 0;  break;

            case ('B'):  para_temp.bit_rate = 1;  break;
            case ('F'):  para_temp.can_type = 1;  break;
            case ('E'):  para_temp.enable = 1;    break;
            case ('S'):  para_temp.send_res = 1;  break;
            case ('R'):  para_temp.res_flag = 1;  break;
            case ('A'):  para_temp.send_acc = 1;  break;
            }
        }

        for (uint8_t i = 0; i < can_num; i++)
        {
            const uint8_t canx_index = canx[i] - 1;

            fdcan_config[canx_index].s.nomi.baud = nomi.baud;
            fdcan_config[canx_index].s.nomi.pre = nomi.pre;
            fdcan_config[canx_index].s.nomi.sjw = nomi.sjw;
            fdcan_config[canx_index].s.nomi.tseg1 = nomi.tseg1;
            fdcan_config[canx_index].s.nomi.tseg2 = nomi.tseg2;

            fdcan_config[canx_index].s.data.baud = data.baud;
            fdcan_config[canx_index].s.data.pre = data.pre;
            fdcan_config[canx_index].s.data.sjw = data.sjw;
            fdcan_config[canx_index].s.data.tseg1 = data.tseg1;
            fdcan_config[canx_index].s.data.tseg2 = data.tseg2;

            fdcan_config[i].s.para.bit_rate = para_temp.bit_rate;
            fdcan_config[i].s.para.can_type = para_temp.can_type;
            fdcan_config[i].s.para.enable = para_temp.enable;
            fdcan_config[i].s.para.send_res = para_temp.send_res;
            fdcan_config[i].s.para.res_flag = para_temp.res_flag;
        }
        
        Fdcan_Init();
        print("C set cus ok\r\n");
        return;
    }
    else if (strncmp(p_str, "init", 4) == 0)  // 初始化
    {
        p_str += 5;
        if (Config_Set_Inspect_Canx(&p_str, canx, &can_num) != 0)
        {
            return;
        }

        const uint8_t enacle_temp = Config_Ascii_To_Int(&p_str);
        for (uint8_t i = 0; i < can_num; i++)
        {
            fdcan_config[i].s.para.enable = enacle_temp;
        }

        Fdcan_Init();
        print("C set init ok\r\n");
        return;
    }
    else if (strncmp(p_str, "write", 5) == 0)  // 保存为默认配置
    {
        p_str += 6;
        Fdcan_Config_Write();

        print("C set write ok\r\n");
        return;
    }

    print("C set err\r\n");
}


void Config_Handle(char *p_str)
{
    if (p_str == NULL)
    {
        print("C err\r\n");
        return;
    }

    if (strncmp(p_str, "get", 3) == 0)
    {
        p_str += 4;
        Config_Get(p_str);
    }
    else if (strncmp(p_str, "set", 3) == 0)
    {
        p_str += 4;
        Config_Set(p_str);
    }
}
