#ifndef _FDCAN_CONFIG_H
#define _FDCAN_CONFIG_H



#include "qlk_usart.h"


#define  FDCAN_CONFIG_ADDR  0
#define  FDCAN_CLK_MHZ      80


typedef struct
{
    uint8_t pre;
    uint8_t sjw;
    uint8_t tseg1;
    uint8_t tseg2;
    uint16_t baud;
} fdcan_config_baud_s, *p_fdcan_config_baud_s;


typedef struct 
{
    uint8_t enable;     // 使能与否
    uint8_t can_type;   // CAN 类型：FDCAN 或 CAN2.0（0-CAN2.0，1-FDCAN）
    uint8_t bit_rate;   // 是否使用比特率切换（0-否，1-是）
    uint8_t res_flag;   // 是否使能终端电阻
    uint8_t send_res;   // 自动重发（目前会一直重发，待硬件更新后增加改成发送到总线关闭模式）
    uint8_t send_acc;   // 发送反馈
} fdcan_config_para_s, *p_fdcan_config_para_s;


typedef struct
{
    fdcan_config_baud_s nomi;
    fdcan_config_baud_s data;
    fdcan_config_para_s para;
} fdcan_config_ss, *p_fdcan_config_ss;


typedef struct 
{
    union 
    {
        fdcan_config_ss s;
        uint64_t data64[(sizeof(fdcan_config_ss) / 8) + 1];
    };
} fdcan_config_s, *p_fdcan_config_s;



typedef struct 
{
    FDCAN_HandleTypeDef *p_fdcan_x;
    FDCAN_GlobalTypeDef *p_instance;
} fdcan_config_handle_s, *p_config_fdcan_handle_s;

extern const fdcan_config_handle_s fdcan_config_handle[];

uint8_t Fdcan_Config_Get_Acc(const uint8_t x);
void Fdcan_Init(void);
void Fdcan_Config_Read(void);
void Config_Handle(char *p_str);


#endif
