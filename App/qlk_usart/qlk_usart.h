#ifndef _QLK_USART_H
#define _QLK_USART_H


#include "main.h"
#include "usbd_cdc_if.h"
#include "qlk_fdcan.h"
#include <stdarg.h>


#define DEBUG_FLAG
#ifdef  DEBUG_FLAG
void print(const char *format, ...);
#define DEBUG_PRINT(format, ...) print(format"\r\n", ##__VA_ARGS__)
#else
#define DEBUG_PRINT(x, ...)
#endif


#define  USART_BUF_LEN    160  // 每个 USART 缓存的最大大小
#define  USART_QUEUE_LEN  10  // 每个 USART 队列缓存的数量


typedef struct
{
    uint8_t data[USART_BUF_LEN];
    uint16_t len;
} usart_buf;


typedef struct
{
    usart_buf buf[USART_QUEUE_LEN];
    uint8_t push;
    uint8_t pop;
} queue_usart;


extern queue_usart queue_usart1;


void Usart_Send(uint8_t *data, uint16_t len);
uint8_t Usart_Queue_Push(uint8_t *data, uint16_t len);
uint8_t Usart_Queue_Pop(uint8_t *data);



#endif
