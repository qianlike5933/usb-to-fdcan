#include "qlk_usart.h"



queue_usart queue_usart1 =
{
    .push = 0,
    .pop = 0,
};

uint8_t fdcan1_tdata[FDCAN_BUF_LEN] = {0};


void Usart_Send(uint8_t *data, uint16_t len)
{
    CDC_Transmit_FS(data, len);
}


uint8_t Usart_Queue_Push(uint8_t *data, uint16_t len)
{
    if (data == NULL)
    {
        DEBUG_PRINT("ERR: Pointer is empty!!!");
        return 1;
    }

    if (len > USART_BUF_LEN || len == 0)
    {
        DEBUG_PRINT("ERR: Data length error!!!");
        return 2;
    }

    memcpy(queue_usart1.buf[queue_usart1.push].data, data, len);
    queue_usart1.buf[queue_usart1.push].len = len;
    if (++queue_usart1.push >= USART_QUEUE_LEN)
    {
        queue_usart1.push = 0;
    }

    if (queue_usart1.pop == queue_usart1.push)
    {
        if (++queue_usart1.pop >= USART_QUEUE_LEN)
        {
            queue_usart1.pop = 0;
        }
    }

    return 0;
}


uint8_t Usart_Queue_Pop(uint8_t *data)
{
    uint8_t len = 0;

    if (queue_usart1.pop == queue_usart1.push)
    {
        //        DEBUG_PRINT("WAR: The queue is empty");
        return 0;
    }

    if (data == NULL)
    {
        DEBUG_PRINT("ERR: Pointer is empty!!!");
        return 0;
    }

    memcpy(data, queue_usart1.buf[queue_usart1.pop].data, queue_usart1.buf[queue_usart1.pop].len);
    len = queue_usart1.buf[queue_usart1.pop].len;
    if (++queue_usart1.pop >= USART_QUEUE_LEN)
    {
        queue_usart1.pop = 0;
    }

    return len;
}

#ifdef DEBUG_FLAG
void print(const char *format, ...)
{
    uint8_t tdata[100];
    uint32_t len = 0;
    va_list args; // 定义可变参数列表变量


    va_start(args, format); // 初始化可变参数列表
    len = vsprintf((char *)tdata, format, args); // 使用可变参数列表
    va_end(args); // 清理可变参数列表

    CDC_Transmit_FS(tdata, len);
}
#endif
