#include "qlk_flash.h"


uint8_t Flash_Erase_Page(uint32_t addr, uint32_t np)
{
    uint32_t addr_temp = addr + FLASH_ADDR_BASE_W;
    uint32_t addr_page = ((addr_temp - FLASH_ADDR_BASE) / FLASH_ADDR_PAGE_SIZE);

    // ��������
    FLASH_EraseInitTypeDef EraseInitStruct;
    uint32_t PAGEError;
    EraseInitStruct.TypeErase = FLASH_TYPEERASE_PAGES;
    EraseInitStruct.Page = addr_page;
    EraseInitStruct.NbPages = np;
    if (addr < 0x08040000)
    {
        EraseInitStruct.Banks = FLASH_BANK_1;
    }
    else
    {
        EraseInitStruct.Banks = FLASH_BANK_2;
    }

    // ����Flashҳ
    if (HAL_FLASHEx_Erase(&EraseInitStruct, &PAGEError) != HAL_OK)
    {
        return 1;
    }
    return 0;
}

void Flash_Write_int64(uint32_t addr, uint64_t *data, uint32_t len)
{
    HAL_FLASH_Unlock(); // ����Flash

	if (Flash_Erase_Page(addr, 1) != 0)
	{
		DEBUG_PRINT("flash erage err");
		HAL_FLASH_Lock();
		return;
	}
    

    for (uint32_t i = 0; i < len; i++)
    {
        if(HAL_FLASH_Program(FLASH_TYPEPROGRAM_DOUBLEWORD, addr + FLASH_ADDR_BASE_W, data[i]) != HAL_OK)
        {
            DEBUG_PRINT("flash write err %d", HAL_FLASH_GetError());
            break; // �����ִ�������ֹѭ��
        }
        addr += 8;
    }
    HAL_FLASH_Lock(); // ����Flash
}


void Flash_Read_int64(uint32_t addr, uint64_t *data, uint32_t len)
{
    while (len--)
    {
        *data = *(__IO uint64_t *)(addr + FLASH_ADDR_BASE_W);
        addr += 8;
        data++;
    }
}




