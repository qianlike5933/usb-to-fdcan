#ifndef _QLK_FLASH_H
#define _QLK_FLASH_H


#include "main.h"


#define FLASH_ADDR_BASE_W      0X0801F800
#define FLASH_ADDR_BASE        0X08000000
#define FLASH_ADDR_PAGE_SIZE   0X800


uint8_t Flash_Erase_Page(uint32_t addr, uint32_t np);
void Flash_Write_int64(uint32_t addr, uint64_t *data, uint32_t len);
void Flash_Read_int64(uint32_t addr, uint64_t *data, uint32_t len);


#endif
