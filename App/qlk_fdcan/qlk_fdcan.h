#ifndef _QLK_FDCAN_H
#define _QLK_FDCAN_H


#include "main.h"
#include "fdcan.h"
#include "qlk_usart.h"


#define  FDCAN_BUF_LEN    64  // 每个FDCAN缓存的最大大小
#define  FDCAN_QUEUE_LEN  10  // 每个FDCAN队列缓存的数量

typedef struct
{
    uint8_t data[FDCAN_BUF_LEN];
    uint8_t len;        // 数据段长度（0~64）
    uint8_t frame_type; // 帧的类型：数据帧或远程帧（0-数据帧，1-远程帧）
    uint8_t bit_rate;   // 是否使用了比特率切换（0-否，1-是）
    uint8_t id_type;    // ID 类型：标准或扩展（0-标准，1-拓展）
    uint8_t can_type;   // CAN 类型：FDCAN 或 CAN2.0（0-CAN2.0，1-FDCAN）
    uint8_t config_flag;// 判断上位机有无指定数据发送的格式（0-使用默认配置，1-使用上位机指定的配置）
    uint8_t port;       // 选择 CAN 通道
    uint16_t time;      // 接收到信息的时间戳(开始，0~0xFFFF)
    uint32_t id;        // ID（标准：0~0x7FF，拓展：0~0x1FFFFFFF）
} fdcan_buf;


typedef struct
{
    fdcan_buf buf[FDCAN_QUEUE_LEN];
    uint8_t push;
    uint8_t pop;
} queue_fdcan;

extern queue_fdcan queue_fdcan1;


void Fdcan_Send(FDCAN_HandleTypeDef *fdcan_x, uint32_t id, uint8_t *data, uint8_t len);  // FDCAN 发送数据
void Fdcan_Send2(FDCAN_HandleTypeDef *fdcan_x, fdcan_buf *p_fdcan_buf, uint8_t acc_flag);
void Fdcan_Port_Send(fdcan_buf *p_fdcan_buf, const uint8_t acc_flag);
uint8_t Fdcan_Queue_Push(FDCAN_RxHeaderTypeDef *p_rx_header, uint8_t *data, uint8_t port, uint8_t len);
uint8_t Fdcan_Queue_Pop(fdcan_buf *p_fdcan_buf);
void Fdcan_To_Usart();

#endif
