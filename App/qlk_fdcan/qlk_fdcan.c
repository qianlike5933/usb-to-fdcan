#include "qlk_fdcan.h"


uint8_t fdcan2_rdata[FDCAN_BUF_LEN] = {0};
queue_fdcan queue_fdcan1 =
{
    .pop = 0,
    .push = 0,
};

static uint32_t Fdcan_Len_To_Dlc(uint8_t len)
{
    uint32_t dlc = 0;

    if (len <= 8)
    {
        dlc = len << 16;
    }
    else if (len <= 12)
    {
        dlc = FDCAN_DLC_BYTES_12;
    }
    else if (len <= 16)
    {
        dlc = FDCAN_DLC_BYTES_16;
    }
    else if (len <= 20)
    {
        dlc = FDCAN_DLC_BYTES_20;
    }
    else if (len <= 24)
    {
        dlc = FDCAN_DLC_BYTES_24;
    }
    else if (len <= 32)
    {
        dlc = FDCAN_DLC_BYTES_32;
    }
    else if (len <= 48)
    {
        dlc = FDCAN_DLC_BYTES_48;
    }
    else if (len <= 64)
    {
        dlc = FDCAN_DLC_BYTES_64;
    }

    return dlc;
}


static uint8_t Fdcan_Dlc_To_Len(uint32_t dlc)
{
    uint8_t len = 0;
    const uint8_t tab_dlc_to_len[] = {12, 16, 20, 24, 32, 48, 64};

    if (dlc <= FDCAN_DLC_BYTES_8)
    {
        len = dlc >> 16;
    }
    else
    {
        len = tab_dlc_to_len[(dlc >> 16) - 9];
    }

    return len;
}


/**
 * @brief FDCAN 发送数据
 * @param fdcan_x 选择 FDCANx
 * @param id id
 * @param data 数据
 * @param len 数据长度
 */
void Fdcan_Send(FDCAN_HandleTypeDef *fdcan_x, uint32_t id, uint8_t *data, uint8_t len)
{
    static FDCAN_TxHeaderTypeDef TxHeader = {0};

    /* 配置发送参数 */
    if (id > 0x7ff)
    {
        TxHeader.IdType = FDCAN_EXTENDED_ID;     		 /* 扩展ID */
    }
    else
    {
        TxHeader.IdType = FDCAN_STANDARD_ID;     		 /* 标准ID */
    }

    TxHeader.Identifier = id;             		     /* 设置接收帧消息的ID */
    TxHeader.TxFrameType = FDCAN_DATA_FRAME;		     /* 数据帧 */
    TxHeader.DataLength = Fdcan_Len_To_Dlc(len);         /* 发送数据长度 */
    TxHeader.ErrorStateIndicator = FDCAN_ESI_ACTIVE;     /* 设置错误状态指示 */
    TxHeader.BitRateSwitch = FDCAN_BRS_OFF;               /* 关闭可变波特率 */
    TxHeader.FDFormat = FDCAN_FD_CAN;                    /* FDCAN格式 */
    TxHeader.TxEventFifoControl = FDCAN_NO_TX_EVENTS;    /* 用于发送事件FIFO控制, 不存储 */
    TxHeader.MessageMarker = 0;     /* 用于复制到TX EVENT FIFO的消息Maker来识别消息状态，范围0到0xFF */

    HAL_FDCAN_AddMessageToTxFifoQ(fdcan_x, &TxHeader, data);
}


void Fdcan_Send2(FDCAN_HandleTypeDef *fdcan_x, fdcan_buf *p_fdcan_buf, uint8_t acc_flag)
{
    static FDCAN_TxHeaderTypeDef TxHeader = {0};

    /* 配置发送参数 */
    if (p_fdcan_buf->config_flag == 1)
    {
        p_fdcan_buf->config_flag = 0;
        TxHeader.IdType = p_fdcan_buf->id_type == 1 ? FDCAN_EXTENDED_ID : 0;
        TxHeader.Identifier = p_fdcan_buf->id;             		     /* 设置接收帧消息的ID */
        TxHeader.TxFrameType = p_fdcan_buf->frame_type == 0 ? FDCAN_DATA_FRAME : 0;		     /* 数据帧 */
        TxHeader.DataLength = Fdcan_Len_To_Dlc(p_fdcan_buf->len);         /* 发送数据长度 */
        TxHeader.ErrorStateIndicator = FDCAN_ESI_ACTIVE;     /* 设置错误状态指示 */
        TxHeader.BitRateSwitch = p_fdcan_buf->bit_rate == 1 ? FDCAN_BRS_ON : 0;               /* 开启可变波特率 */
        TxHeader.FDFormat = p_fdcan_buf->can_type == 1 ? FDCAN_FD_CAN : 0;                    /* FDCAN格式 */
        TxHeader.TxEventFifoControl = FDCAN_NO_TX_EVENTS;    /* 用于发送事件FIFO控制, 不存储 */
        TxHeader.MessageMarker = 0;     /* 用于复制到TX EVENT FIFO的消息Maker来识别消息状态，范围0到0xFF */
    }
    else
    {
        if (p_fdcan_buf->id > 0x7fff)
        {
            TxHeader.IdType = FDCAN_EXTENDED_ID;     		 /* 扩展ID */
        }
        else
        {
            TxHeader.IdType = FDCAN_STANDARD_ID;     		 /* 标准ID */
        }

        TxHeader.Identifier = p_fdcan_buf->id;             		    /* 设置接收帧消息的ID */
        TxHeader.TxFrameType = FDCAN_DATA_FRAME;		            /* 数据帧 */
        TxHeader.DataLength = Fdcan_Len_To_Dlc(p_fdcan_buf->len);   /* 发送数据长度 */
        TxHeader.ErrorStateIndicator = FDCAN_ESI_ACTIVE;     /* 设置错误状态指示 */
        TxHeader.BitRateSwitch = FDCAN_BRS_OFF;               /* 关闭可变波特率 */
        TxHeader.FDFormat = FDCAN_FD_CAN;                    /* FDCAN格式 */
        TxHeader.TxEventFifoControl = FDCAN_NO_TX_EVENTS;    /* 用于发送事件FIFO控制, 不存储 */
        TxHeader.MessageMarker = 0;     /* 用于复制到TX EVENT FIFO的消息Maker来识别消息状态，范围0到0xFF */
    }

    HAL_StatusTypeDef r = HAL_FDCAN_AddMessageToTxFifoQ(fdcan_x, &TxHeader, p_fdcan_buf->data);
    if (acc_flag == 0)
    {
        return;
    }
    
    if (r == HAL_OK)
    {
        Usart_Send((uint8_t *)"S OK\r\n", 6);
    }
    else
    {
        Usart_Send((uint8_t *)"S ERR 1\r\n", 9);
    }
}


void Fdcan_Port_Send(fdcan_buf *p_fdcan_buf, const uint8_t acc_flag)
{
    switch (p_fdcan_buf->port)  // 硬件暂时只引出了一个 fdcan2 口
    {
    case (1):
        Fdcan_Send2(&hfdcan2, p_fdcan_buf, acc_flag);
        break;
    case (2):
        Fdcan_Send2(&hfdcan2, p_fdcan_buf, acc_flag);
        break;
    case (3):
        Fdcan_Send2(&hfdcan2, p_fdcan_buf, acc_flag);
        break;
    default:
        Fdcan_Send2(&hfdcan2, p_fdcan_buf, acc_flag);
        // DEBUG_PRINT("ERR: Canfd port error!!!");
        break;
    }
}


uint8_t Fdcan_Queue_Push(FDCAN_RxHeaderTypeDef *p_rx_header, uint8_t *data, uint8_t port, uint8_t len)
{
    if (data == NULL || p_rx_header == NULL)
    {
        DEBUG_PRINT("ERR: Pointer is empty!!!");
        return 1;
    }

    if (len > FDCAN_BUF_LEN)
    {
        DEBUG_PRINT("ERR: Data length error!!!");
        return 2;
    }

    memcpy(queue_fdcan1.buf[queue_fdcan1.push].data, data, len);
    queue_fdcan1.buf[queue_fdcan1.push].id = p_rx_header->Identifier;
    queue_fdcan1.buf[queue_fdcan1.push].id_type = p_rx_header->IdType == FDCAN_EXTENDED_ID;
    queue_fdcan1.buf[queue_fdcan1.push].bit_rate = p_rx_header->BitRateSwitch == FDCAN_BRS_ON;
    queue_fdcan1.buf[queue_fdcan1.push].can_type = p_rx_header->FDFormat == FDCAN_FD_CAN;
    queue_fdcan1.buf[queue_fdcan1.push].frame_type = p_rx_header->RxFrameType == FDCAN_REMOTE_FRAME;
    queue_fdcan1.buf[queue_fdcan1.push].time = p_rx_header->RxTimestamp;
    queue_fdcan1.buf[queue_fdcan1.push].len = len;
    queue_fdcan1.buf[queue_fdcan1.push].port = port;
    if (++queue_fdcan1.push >= FDCAN_QUEUE_LEN)
    {
        queue_fdcan1.push = 0;
    }

    if (queue_fdcan1.pop == queue_fdcan1.push)
    {
        if (++queue_fdcan1.pop >= FDCAN_QUEUE_LEN)
        {
            queue_fdcan1.pop = 0;
        }
    }

    return 0;
}


uint8_t Fdcan_Queue_Pop(fdcan_buf *p_fdcan_buf)
{
    if (queue_fdcan1.pop == queue_fdcan1.push)
    {
        //        DEBUG_PRINT("WAR: The queue is empty");
        return 1;
    }

    if (p_fdcan_buf == NULL)
    {
        DEBUG_PRINT("ERR: Pointer is empty!!!");
        return 2;
    }

    memcpy(p_fdcan_buf, &(queue_fdcan1.buf[queue_fdcan1.pop]), sizeof(fdcan_buf));
    // memcpy(&(p_fdcan_buf->data[0]), &(queue_fdcan1.buf[queue_fdcan1.pop].data[0]), queue_fdcan1.buf[queue_fdcan1.pop].len);
    // p_fdcan_buf->bit_rate = queue_fdcan1.buf[queue_fdcan1.pop].bit_rate;
    // p_fdcan_buf->can_type = queue_fdcan1.buf[queue_fdcan1.pop].can_type;
    // p_fdcan_buf->config_flag = queue_fdcan1.buf[queue_fdcan1.pop].config_flag;
    // p_fdcan_buf->frame_type = queue_fdcan1.buf[queue_fdcan1.pop].frame_type;
    // p_fdcan_buf->id = queue_fdcan1.buf[queue_fdcan1.pop].id;
    // p_fdcan_buf->id_type = queue_fdcan1.buf[queue_fdcan1.pop].id_type;
    // p_fdcan_buf->len = queue_fdcan1.buf[queue_fdcan1.pop].len;
    // p_fdcan_buf->time = queue_fdcan1.buf[queue_fdcan1.pop].time;
    if (++queue_fdcan1.pop >= FDCAN_QUEUE_LEN)
    {
        queue_fdcan1.pop = 0;
    }

    return 0;
}



void HAL_FDCAN_RxFifo0Callback(FDCAN_HandleTypeDef *hfdcan, uint32_t RxFifo0ITs)  // FDCAN FIFO 0 回调函数
{
    uint8_t len = 0;
    FDCAN_RxHeaderTypeDef fdcan_rx_header2;

    if(hfdcan->Instance == FDCAN2)
    {
        HAL_FDCAN_GetRxMessage(hfdcan, FDCAN_RX_FIFO0, &fdcan_rx_header2, fdcan2_rdata);
        len = Fdcan_Dlc_To_Len(fdcan_rx_header2.DataLength);
        Fdcan_Queue_Push(&fdcan_rx_header2, fdcan2_rdata, 1, len);
    }
}

